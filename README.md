# Liant's Rails template 

## Introduction

This is a simple application with some predefined default plug-ins installed:
- Rspec
- Devise (configured with local views)
- Bootstrap views

## État du projet

[![pipeline status](https://gitlab.com/liant-sasu/rails-base-project/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/liant-sasu/rails-base-project/-/commits/main)
[![coverage report](https://gitlab.com/liant-sasu/rails-base-project/badges/main/coverage.svg)](https://gitlab.com/liant-sasu/rails-base-project/-/commits/main)
[![Latest Release](https://gitlab.com/liant-sasu/rails-base-project/-/badges/release.svg)](https://gitlab.com/liant-sasu/rails-base-project/-/releases)

### Déploiement

Le déploiement a lieu dans le cloud Liant à l'adresse `https://rails-base-project.liant.cloud`
pour la release principale et sur un lien créé à partir du nom de la branche comme
ceci : https://\<branch\>.rails-base-project.liant.cloud. Le mot \<branche\> vient de la variable
prédéfinie par GitLab **CI_ENVIRONMENT_SLUG** qui lui même est configuré pour contenir
`review/$CI_COMMIT_REF_SLUG` tronqué à 24 caractères avec un suffix aléatoire de 6
caractères. **CI_COMMIT_REF_SLUG** est quant à lui construit à partir du nom de la branche ou tag.

Un exemple est le premier environnement créé qui était à l'adresse :
`https://review-feature-me-981wv8.manager.anc.liant.cloud/`

## Développement

### Dépendances

- Postgres (`libpg`)

### MacOS

Sur MacOS, il est nécessaire d'installer avec `brew` par exemple la
bibliothèque libpg, puis configurer bundle pour l'utiliser correctement.

    brew install libpg
    bundle config build.pg \
        --with-pg-config=/opt/homebrew/opt/libpq/bin/pg_config
    bundle

## Duplicate the template and use it

### Copy the project

Here we aim to make a new project. So we purposely use a method that will
get ride of git's history.

If you prefere to get update, then it will be better to fork the project, but
then it will be tricky to merge the change in this project in yours anyway.

#### On GitLab.org

First, download the current archive from Gitlab.org [the archive](https://gitlab.com/liant-sasu/rails-base-project/-/archive/main/rails-base-project-main.tar.bz2).

Extract it in the folder you want and change the name of the folder to one of your liking (ex: "MyProjectName"):

    tar xjf rails-base-project-main.tar.bz2 <your destination>/
    cd <your destination>/
    mv rails-base-project-main MyProjectName

On Gitlab.org, create a new empty project. Set its name and all other parameters you want. Do not let the GitLab initialize the project with the readme.

GitLab guide you to initialize the git repository. The only information you need is the remote location command, which should looks like:

    git remote add origin git@gitlab.com:<your-name>/<your project slug>.git

This is the third line on the "Push an existing project section".

#### On your computer

Since we don't want to get a git historic of all the following
modifications, we will create and init the git repository later.
Don't worry, but don't do things without testing your steps (except
what we tell you to do here which are a little hard to test at the
beggining).

##### Database

Known errors: No postgresql local installation, please install it before
playing with this website.

- Go to config/database.yml and rename your database for your project.

- (not database related, but rails won't work because it will not find
    the credentials) Remove reference to credential content:
    - comment in config/environments/development.rb the lines 50 an 51.

- Run the setup of your database locally:

        bundle exec rails db:setup

- As you will see in the logs, it will create 2 users (see
    the db/seeds.rb file to change this). We will use them to test the
    website locally after.

##### Customize your credentials

- Create a new secret file (See [Rails Guide for more details](https://guides.rubyonrails.org/configuring.html#config-credentials-content-path)):

        rm config/credentials.yml.enc
        bundle exec rails credentials:edit

    This step creates a new file `config/credentials.yml.enc`, and show it to
    you using your $VISUAL or $EDITOR terminal (as explained on Rails guide).
    It will also create a file called `config/master.key` containing your
    encoding key for the credentials file.

- Inside the credentials, add the smtp credentials by adding a section like this:

        smtp:
          user: my_server@example.com
          password: <a very long and random password of at least 32 characters>
    This password and user must be monitored for leaks (like by the one
    included in Chrome, Firefox, MacOs or any other inteligent Keychains),
    because it is a simple user/password pair.
    After this, you can uncomment the line (50 and 51) in
    `config/environments/development.rb` that we commented on the previous
    section.

- For your development environment, in the files
    `config/environments/development.rb`:
    - line 47, update the parameters of your email service. Using Google mail can be
    complicated to setup since they don't accept simple user/login password,
    then don't restrain from
    [ordering us](https://liant.shop/boutique/services/en-ligne/adresse-courriel/)
    a "Simple" email address for your server on your domain this a way to
    support us and it is not that expansive (2.48 €/month on our domain, or
    add 9€ at the installation to make it work on your own domain).
    - line 89, the local server is configured to be on localhost (IPv6) and
    port 3000 to make your emails locally sent to generate address you can use
    during your tests.

- For your production environment, in the file
    `config/environments/production.rb`:
    - line 83, update the parameters of your email service.
    - About the host configuration `config.action_mailer.default_url_options`
        setup in the case of the production environment, it is automatically
        made in the file config/application.rb:48 and following lines.
    - line 104, setup the URL allowed to prevent Host Header attacks.

- Change the default email sender:
    - in the file `app/mailers/application_mailer.rb`, line 5.
    - in the file `config/initializers/devise.rb` line 28.
    - you can also update the realm used in Http Basic Authentication
        line 89 in `config/initializers/devise.rb`.

- Then you can test your application by running Rails server:

        bundle exec rails s

    Go to http://[::1]:3000 (please try to think IPv6 before IPv4), and enjoy your new Website.

- You can test the to sign_in actions and registering actions now.


##### Cosmetics

- Edit the README to make it fit a basic description of your project.
    - Think to update the badges by editing your project path.

- Edit the LICENSE to make it fit the one you'd like for your project.
    We recommend to go to
    ["Choose a license"](https://choosealicense.com/licenses/) to
    choose your license.

- Edit the Logo of the application: app/assets/images/logos/logo.png
    - Then use the bin/generate_favicons.sh script to generate the 
    favicons of various sizes (16 24 32 48 64 76 96 120 128 152 167
    180 192 256 384 512). Obviously, if you start with high-resolution
    logo, it will make better favicons (start with at least more than
    500000 pixels). The script is compatible with some linux
    distributions and MacOs with `brew` installed because it will
    try to install the necessary commands to work.

- Rework you welcome page:
    - change the app/views/welcome/index.html.haml
    - change the app/assets/stylesheets/welcome.sass
    - don't forget to update the config/locales/[en|fr].yml files

- Remove the logos you don't need they were used only in the file
    `app/views/welcome/index.html.haml`.

- Edit `config/locales/en.yml` and `config/locales/fr.yml` app.name field
    to make it fit your application name.

- Change the name of the deployment agent: rename the folder
    `.gitlab/agents/rails-base-project-liant-cloud` to an agent name that
    fits your naming policies of your cluster (for us it is the
    `<project-name>-<cluster-name>`).
    Then edit `.gitlab-ci.yml`:
    - Line 13, change the KUBE_CONTEXT to set the path to the agent. In our
        case, it is `$CI_PROJECT_PATH:rails-base-project-liant-cloud`, you have
        the project path part, then after the `:`, the agent's name. You should
        just have to update the agent's name.
    - Line 14, change the namespace on which you want to publish your
        application (we make it match the application slug from the
        CI_PROJECT_PATH_SLUG available by GitLab CI),
    - Line 22, change the subdomain you want to use for your porject too.

- Make sure that the tests still pass after your changes:

        bundle exec rspec

- Make sure rubocop is happy with you coding:

        ./bin/rubocop

##### Git and push

Now we should initialize git repository and push it to Gitlab, even if it is
not totally ready for deployment, but the following steps needs the repository
to have something in it.

Then follow those steps:

    git init
    git add .
    git commit -m 'Initial commit of the project'
    git remote add origin git@gitlab.com:<your-name>/<your project slug>.git
    git push --set-upstream origin main

As [we said earlier](#### On GitLab.org), the remote address is given in
GitLab.org interface.

![New project présentation page](app/assets/images/New%20Empty%20Project.png)

As you will see in the Pipelines, the deployment will be mising yet (or will
fail).

##### Kubernetes

Now you have a locally working application. At this moment, the deployment
to your Kubernetes cluster is not ready.

First thing is to configure your cluster (if you need help on that we can
provide support
[here](https://liant.services/landing-page/service-dingenieur-devops/)),
we assume here that you have the following helm already installed:
- ingress (that should have gave you an IP on which you have routed your
    domain name or service name and subdomains on it)
- cert-manager (allowing to get https certificates easily)

For GitLab integration, you need to setup the
[GitLab Agent](https://docs.gitlab.com/ee/user/clusters/agent),
you'll find the instruction
[here](https://docs.gitlab.com/ee/user/clusters/agent/install), or you can
follow our lead again:

[As we said earlier](##### Git and push), since the project already expect an
agent, a configuration file that you already renamed is present in the
repository (this is step 1 of GitLab's Guide). The configuration file is
empty (`config.yaml`), because we don't need specific configuration in our
case.

Step 2 is in GitLab's interface: create the agent from GitLab platform point
of view. In the interface of your project, go to
`Operate > Kubernetes clusters` in the project, then click on
`Connect a cluster (agent)` top right.

![Create the cluster agent - 1](app/assets/images/Create%20the%20cluster%20agent%20-%201.png)

This displays to you a first step to create or use your configuration file.

![Create the cluster agent - 2](app/assets/images/Create%20the%20cluster%20agent%20-%202.png)

Here we select the existing agent and save it. This will display an other step describing what you need to do on your cluster:

![Create the cluster agent - 3](app/assets/images/Create%20the%20cluster%20agent%20-%203.png)

Choose in which namespace to put your agent, regarding your policy, and then
launch the `helm upgrade -- install xxx` command.

After closing the modal window, you will see your agent as never connected in
the GitLab interface.

![Create the cluster agent - 4](app/assets/images/Create%20the%20cluster%20agent%20-%204.png)

But once your cluster has correclty assigned the pods and the agent is running,
it will connect properly and it will be visible on the Gitlab interface.

![Create the cluster agent - 5](app/assets/images/Create%20the%20cluster%20agent%20-%205.png)

If you don't have things to commit, just trigger a new pipeline and all the
deployment steps should now occurs.

##### Troubleshooting:

- When testing my tutorial, GitLab was on the verge to release the v17.1.0 version of the agenttk.
    This means that my cluster was unable to get the Pull the image from their registry,
    and I got this kind of logs:

          Normal   BackOff    17s (x20 over 5m21s)   kubelet            Back-off pulling image "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:v17.1.0"
    
    I then went to check the available tags on [their registry](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/container_registry/), and rolledback to the v17.0.0.

    Since I saw that after launching the command:

        helm upgrade --install rails-base-project-liant-cloud gitlab/gitlab-agent \                     
            --namespace gitlab-agents \
            --create-namespace \
            --set image.tag=v17.1.0 \
            --set config.token=<this token is mine> \
            --set config.kasAddress=wss://kas.gitlab.com

    I just had to relaunch the command changing the tag (no need to clean the previous
    installation):

        helm upgrade --install rails-base-project-liant-cloud gitlab/gitlab-agent \                     
            --namespace gitlab-agents \
            --create-namespace \
            --set image.tag=v17.0.0 \
            --set config.token=<this token is mine> \
            --set config.kasAddress=wss://kas.gitlab.com


Finally, check our group and project environment variables to duplicate them and set them at
the values you need. Notably, there is the `K8S_SECRET_RAILS_MASTER_KEY` variable that must be
present for deployment to work. 

Now that your Kubernetes agent is configured, the next pipeline triggered will try to deploy
your application.
