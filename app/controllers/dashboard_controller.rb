# frozen_string_literal: true

# Controller du dasboard.
class DashboardController < ApplicationController
  before_action :authenticate_user!

  def index; end
end
