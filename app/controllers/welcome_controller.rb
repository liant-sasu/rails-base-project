# frozen_string_literal: true

require 'devise/version'

# This is the Welcome page parameter
class WelcomeController < ApplicationController
  def index; end
end
