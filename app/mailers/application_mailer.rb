# frozen_string_literal: true

# Application level Mailer configuration and management.
class ApplicationMailer < ActionMailer::Base
  default from: 'references@liant.cloud'
  layout 'mailer'
end
