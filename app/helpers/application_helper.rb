# frozen_string_literal: true

# This module will host Application level helpers.
module ApplicationHelper
  def flash_class(level)
    case level
    when 'success'
      'alert alert-success'
    when 'error', 'alert'
      'alert alert-danger'
    when 'warn'
      'alert alert-warning'
    # Notice case included in the else case.
    # when 'notice'
    #   'alert alert-info'
    else
      'alert alert-info'
    end
  end

  def form_email_field(form)
    form.email_field(
      :email,
      autofocus: true, autocomplete: 'email',
      class: 'form-control',
      value: (resource.pending_reconfirmation? ? resource.unconfirmed_email : resource.email)
    )
  end

  def home_link(...)
    if user_signed_in?
      link_to(:dashboard, ...)
    else
      link_to(:root, ...)
    end
  end
end
