# frozen_string_literal: true

# This module will host Application level helpers.
module UsersHelper
  def form_email_field(form, tag_name = :email)
    form.email_field(
      tag_name,
      autofocus: true, autocomplete: 'email',
      class: 'form-control',
      value: (resource.pending_reconfirmation? ? resource.unconfirmed_email : resource.email)
    )
  end
end
