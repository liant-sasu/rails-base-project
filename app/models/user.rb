# frozen_string_literal: true

# Cette classe décrie les utilisateurs de la plateforme
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  #
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :lockable, :timeoutable, :trackable # , :omniauthable
end
