# frozen_string_literal: true

# This is the parent class for all models.
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
end
