# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Dashboards' do
  context 'without session' do
    describe 'GET /dashboard/' do
      it 'returns http redirected' do
        get '/dashboard/'
        expect(response).to have_http_status(:redirect)
      end
    end
  end

  context 'with session' do
    login_user

    let(:user) { create(:user) }

    describe 'GET /dashboard' do
      it 'returns http sucess' do
        get '/dashboard/'
        expect(response).to have_http_status(:success)
      end
    end
  end
end
