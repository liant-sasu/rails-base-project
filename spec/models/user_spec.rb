# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User do
  it 'can be created with FactoryBot' do
    expect(create(:user)).to be_truthy
  end
end
