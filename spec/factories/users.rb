# frozen_string_literal: true

FactoryBot.define do
  factory :unconfirmed_user do
    email { Faker::Internet.email }
    password { 'Ab0123456789' }
    password_confirmation { 'Ab0123456789' }
  end

  factory :user, aliases: %i[confirmed_user] do
    email { Faker::Internet.email }
    password { 'Ab0123456789' }
    password_confirmation { 'Ab0123456789' }

    after(:create, &:confirm)
  end
end
