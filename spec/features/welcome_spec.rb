# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Welcomes' do
  context 'when visitor is not signed in' do
    it 'displays welcome page' do
      visit welcome_index_path

      expect(page).to have_current_path(welcome_index_path)
    end
  end

  context 'when visitor is signed in' do
    let(:user) { create(:confirmed_user) }

    before do
      visit new_user_session_path

      fill_in :user_email, with: user.email
      fill_in :user_password, with: 'Ab0123456789'
      find('form.new_user input.btn').click
    end

    it 'displays without redirecting' do
      visit welcome_index_path

      expect(page).to have_current_path(welcome_index_path)
    end
  end
end
