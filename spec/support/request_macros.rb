# frozen_string_literal: true

module RequestMacros
  def login_user
    # Before each test, create and login the user
    before do
      user = FactoryBot.create(:user) if user.nil?
      user.confirm
      sign_in(user)
    end
  end
end
