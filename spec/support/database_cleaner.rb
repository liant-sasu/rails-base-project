# frozen_string_literal: true

DatabaseCleaner.url_allowlist = [
  'postgres://user:testing-password@postgres:5432/test',
  'postgres://user:testing-password@localhost:5432/test',
  nil
]
