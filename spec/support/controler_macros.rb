# frozen_string_literal: true

module ControllerMacros
  def login_user
    # Before each test, create and login the user
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      sign_in(user || FactoryBot.create(:user))
    end
  end
end
