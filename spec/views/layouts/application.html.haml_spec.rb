# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'layouts/application' do
  context 'without flash' do
    it "doesn't render the alert row" do
      render
      expect(rendered).to have_no_css('.row.alerts')
    end
  end

  context 'with flash' do
    before { flash[kind] = "A message of #{kind}" }

    {
      'success' => { classes: '.alert.alert-success', message: 'A message of success' },
      'error' => { classes: '.alert.alert-danger', message: 'A message of error' },
      'alert' => { classes: '.alert.alert-danger', message: 'A message of alert' },
      'warn' => { classes: '.alert.alert-warning', message: 'A message of warn' },
      'notice' => { classes: '.alert.alert-info', message: 'A message of notice' }
    }.each do |alert, alert_content|
      describe "[:#{alert}]" do
        let(:kind) { alert }

        it 'renders the alert row' do
          render
          expect(rendered).to have_css('.row.alerts')
        end

        it 'renders the alert message' do
          render

          expect(rendered).to have_css(".row.alerts > div#{alert_content[:classes]}", text: alert_content[:message])
        end
      end
    end
  end
end
